import { html, css, LitElement } from 'lit-element';
import '@polymer/paper-input/paper-input';
import '@polymer/iron-label/iron-label';
import '@polymer/paper-button/paper-button';

export class AddressTracker extends LitElement {
  static get styles() {
    return css`
    /* Mobile */
    @media only screen and (max-width: 375px) {
      #head {
        background-image: url('../src/images/pattern-bg.png');
        text-align: center;
        position: relative;
        height: 150px;
      }

      #detail{
        background: rgb(255, 255, 255);
        border-radius: 18px;
        position: absolute;
        height: 180px;
        z-index: 100;
        display: none;
        box-shadow: rgb(0 0 0 / 50%) 5px 5px 15px;
        text-align: center;
        padding: 10px;
        bottom: 10px;
        top: 22%;
        align-items: center;
        justify-content: center;
        width: 90%;
      }
  
      .headerDivider:nth-child(1) {
        border:none;
      }

      .search{
        border-radius: 8px 0px 0px 8px;
        height: 35px;
        width: 50%;
        padding: 8px;
        border: 0.2px solid gray;
        outline: none;
        font-size: 12px;
      }

      .headerDivider{
        border-left: 2px solid gray;
        position: absolute;
        display: contents;
      }

      #map {
        height: 400px;
        position: relative;
        display: none;
      }

      h4{
        position: relative;
        display: block;
        margin: 10px;
        color: gray;
        font-size: 10px;
      } 

      label{
        font-size: 10px;
      }
  
    }
    
    /* Desktop */
    @media only screen and (min-width: 1400px) {
      #head {
        background-image: url('../src/images/pattern-bg.png');
        text-align: center;
        position: relative;
        height: 200px;
      }

      #detail{
        background: rgb(255, 255, 255);
        border-radius: 18px;
        position: absolute;
        height: 130px;
        z-index: 100;
        display: none;
        grid-template-columns: repeat(4, 170px);
        grid-template-rows: 130px;
        box-shadow: rgb(0 0 0 / 50%) 5px 5px 15px;
        text-align: center;
        padding: 10px;
        bottom: 10px;
        top: 20%;
        left: 25%;
        align-items: center;
        justify-content: center;
      }
  
      .headerDivider:nth-child(1) {
        border:none;
      }

      .search{
        border-radius: 8px 0px 0px 8px;
        height: 35px;
        width: 50%;
        padding: 8px;
        border: 0.2px solid gray;
        outline: none;
        font-size: 12px;
      }

      .headerDivider{
        border-left: 2px solid gray;
      }

      #map {
        height: 500px;
        position: relative;
        display: none;
      }

      h4{
        position: relative;
        display: block;
        margin: 10px;
        color: gray;
      } 
    }

    paper-button{
      background-color: black;
      margin: 0px;
      height: 53px;
      border-radius: 0px 8px 8px 0px;
      border-left: none;
      color: #fff;
    }

    #paper-label{
      padding: 8px;
      display: flow-root;
      color: white;
      font-size: 30px;
    }

    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;
  }

  async __searchIP() {
    const map = this.shadowRoot.querySelector('#map');
    const detail = this.shadowRoot.querySelector('#detail');
    const ip = this.shadowRoot.querySelector('#ip').value;
    const headerDivider1 = this.shadowRoot.querySelectorAll('.headerDivider')[0];
    const headerDivider2 = this.shadowRoot.querySelectorAll('.headerDivider')[1];
    const headerDivider3 = this.shadowRoot.querySelectorAll('.headerDivider')[2];
    const headerDivider4 = this.shadowRoot.querySelectorAll('.headerDivider')[3];

    map.style.display = "block";
    detail.style.display = "grid";
    fetch(`http://ip-api.com/json/${ip}`)
      .then(response => response.json())
      .then(data => {
        headerDivider1.innerHTML = `<h4>Dirección IP</h4><label>${data.query}</label>`;
        headerDivider2.innerHTML = `<h4>Lugar</h4><label>${data.city}, ${data.country}</label>`;
        headerDivider3.innerHTML = `<h4>Zona Horaria</h4><label>${data.timezone}</label>`;
        headerDivider4.innerHTML = `<h4>PSI</h4><label>${data.isp}</label>`;
        new google.maps.Map(map, {
          center: { lat: data.lat, lng: data.lon },
          zoom: 13,
          title: 'Destino'
        });
      })
  }

  render() {
    return html`
      <div id="head">
        <iron-label id="paper-label">IP Address Tracker</iron-label>
        <input placeholder="Search for any IP address or domain" type="text" class="search" id="ip"/><paper-button @click="${this.__searchIP}"><h3>></h3></paper-button>
      </div>
      <div id="detail">
        <div class="headerDivider"></div>
        <div class="headerDivider"></div>
        <div class="headerDivider"></div>
        <div class="headerDivider"></div>
      </div>
      <div id="map"></div>
    `;
  }
}
