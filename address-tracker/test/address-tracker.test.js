import { html, fixture, expect } from '@open-wc/testing';

import '../address-tracker.js';

describe('AddressTracker', () => {
  it('has a default title "Hey there" and counter 5', async () => {
    const el = await fixture(html`<address-tracker></address-tracker>`);

    expect(el.title).to.equal('Hey there');
    expect(el.counter).to.equal(5);
  });

  it('increases the counter on button click', async () => {
    const el = await fixture(html`<address-tracker></address-tracker>`);
    el.shadowRoot.querySelector('button').click();

    expect(el.counter).to.equal(6);
  });

  it('can override the title via attribute', async () => {
    const el = await fixture(html`<address-tracker title="attribute title"></address-tracker>`);

    expect(el.title).to.equal('attribute title');
  });

  it('passes the a11y audit', async () => {
    const el = await fixture(html`<address-tracker></address-tracker>`);

    await expect(el).shadowDom.to.be.accessible();
  });
});
